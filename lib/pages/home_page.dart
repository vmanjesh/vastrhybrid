import 'dart:convert';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:Vastr/components/grid_home.dart';
import 'package:Vastr/components/horizontal_list.dart';
import 'package:Vastr/constant/constants.dart';
import 'package:webview_flutter/webview_flutter.dart';




class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
class _HomePageState extends State<HomePage> {

  final _key = UniqueKey();
  Color color;

  @override
  void initState() {
    super.initState();

    color = Colors.transparent;
  }

  Future<void> _showChoiceDialog(BuildContext context){
    return showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        backgroundColor: Colors.transparent,
        content: SingleChildScrollView(
          child: Container(
            width: 350,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 580,
                  width: 320,
                  padding: EdgeInsets.only(
                    top: 18.0,
                  ),
                  margin: EdgeInsets.only(top: 13.0,right: 8.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.only(topRight: Radius.circular(8), topLeft: Radius.circular(8)),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 0.0,
                          offset: Offset(0.0, 0.0),
                        ),
                      ]),
                  child: Container(
                    margin: EdgeInsets.only(top: 10),
                    height: 420,
                    width: 320,
                    color: Colors.black,
                    child: WebView(
                    key: _key,
                    initialUrl: 'http://vastr.online/',
                    javascriptMode: JavascriptMode.unrestricted,
                  )
                  ),
                ),
                Positioned(
                  right: 8.0,
                  top: 13.0,
                  child: GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Icon(Icons.close, color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.black,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.black,
        title: Stack(
          children: <Widget>[
            Text('VASTR ',
              style: TextStyle(
                  fontFamily: 'FunCity',
                  fontSize: 20,
                  color: Colors.white
              ),
            ),
            Positioned(
              right: 3.5,
              child: Container(
                height: 10,
                width: 10,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  border: Border.all(color: Colors.white,
                    width: 0.5
                  ),

                ),
                child: Text(
                  'TM',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 4
                  ),
                ),
              )
            )
          ],
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              /*Scaffold.of(context).showSnackBar(new SnackBar(
                  content: new Text("value")
              ));*/
            },
            iconSize: 25,
          ),
          IconButton(
            icon: Icon(
              Icons.favorite_border,
              color: Colors.white,
            ),
            onPressed: () {},
            iconSize: 25,
          ),
          shoppingCart(),
        ],
      ),
      drawer: Drawer(
        child: Container(
          child: Center(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 60,
                    color: Colors.grey,
                    padding: EdgeInsets.only(top: 20, left: 25),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.phone_android, color: Colors.white,),
                        SizedBox(width: 15,),
                        Text("Welcome Guest\n Login!", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 7,
                  child: Container(
                    color: Colors.black,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 13,),
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              color = Colors.white;
                            });
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.home, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('Search By Category', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: (){
                            setState(() {
                              color = Colors.white;
                            });
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(MY_ADDRESS);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.business, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('My Addresses', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(MY_ORDER);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.shopping_basket, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('My Order', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(MY_CART);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.shopping_cart, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('My Cart', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(MY_PROFILE);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.person, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('My Profile', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(OFFER_ZONE);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.local_offer, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('Offer Zone', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(CUST_SPRT);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.call, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('Customer Support', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
//                            Navigator.of(context).pushNamed(TERMS_CDN);
                            _showChoiceDialog(context);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.info_outline, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('Term and Conditions', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                        SizedBox(height: 3,),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                            final snackBar = SnackBar(
                                content: Text(
                                    'You are logged out!'
                                ),
                              backgroundColor: Colors.redAccent,
                              behavior: SnackBarBehavior.floating,
                            );
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                          },
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Icon(Icons.power_settings_new, color: Colors.white, size: 30,),
                                SizedBox(width: 15,),
                                Text('Logout', style: TextStyle(color: Colors.white, fontSize: 15),)
                              ],
                            ),
                          ),
                        ),
                        Divider(color: Colors.white,),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 5, right: 5),
        children: <Widget>[
          Container(
              height: 100,
              child: HorizontalList()
          ),
          _banner,
          SizedBox(height: 10),
          t_shirtLabel,
          SizedBox(height: 10),
          Container(
            height: 340.0,
            child: Products(),
          ),
          SizedBox(height: 10),
          firstCarousel,
          SizedBox(height: 10),
          shirtLabel,
          SizedBox(height: 10),
          Container(
            height: 340.0,
            child: Products2(),
          ),
          SizedBox(height: 10),
          favorite_pro,
          SizedBox(height: 10),
          vastr_favorite,
          SizedBox(height: 10),
          Container(
              height: 250,
              margin: EdgeInsets.only(left: 5, right: 5),
              child: HistoryProduct()
          ),
          SizedBox(height: 10),
          secondCarousel,
          SizedBox(height: 10),
          also_expose,
          SizedBox(height: 10),
          Container(
              height: 240,
              child: ProductsExpo()
          )
        ],
      ),
    );
  }

//  <----------==================Here Define others Widgets==============------------>

  Widget _banner = Container(
    height: 300,
    margin: EdgeInsets.only(left: 5, right: 5),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        color: Colors.white
    ),
    child: Container(
      margin: EdgeInsets.only(top: 35, bottom: 35),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          border: Border.all(color: Colors.white),
          image: DecorationImage(
            image: NetworkImage('http://www.projects.estateahead.com/vastr/assets/images/image1.png'),
            fit: BoxFit.fill,
          )
      ),
      height: 300,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              color: Colors.transparent,
              child: InkWell(
                onTap: (){
                  print("Men");
                },
              ),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.transparent,
              child: InkWell(
                onTap: (){
                  print("Women");
                },
              ),
            ),
          )
        ],
      ),
    ),
  );

  Widget firstCarousel = Container(
    height: 300,
    width: 352,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    child: ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(8)),
      child: Carousel(
        images: [
          NetworkImage("http://www.projects.estateahead.com/vastr/assets/images/banner-3.jpg"),
          NetworkImage("http://www.projects.estateahead.com/vastr/assets/images/bag_banner2.jpg"),
          NetworkImage("http://www.projects.estateahead.com/vastr/assets/images/footwear2.jpg"),
        ],
        showIndicator: false,
        animationCurve: Curves.fastOutSlowIn,
      ),
    ),
  );
  Widget secondCarousel = Container(
    height: 300,
    width: 352,
    margin: EdgeInsets.only(left: 10, right: 10),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    child: ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(8)),
      child: Carousel(
        images: [
          NetworkImage("http://www.projects.estateahead.com/vastr/assets/images/t-shirt2.jpg"),
          NetworkImage("http://www.projects.estateahead.com/vastr/assets/images/t-shirt.jpg"),
        ],
        showIndicator: false,
        animationCurve: Curves.fastOutSlowIn,
      ),
    ),
  );

  Widget t_shirtLabel = Card(
    color: Colors.blueGrey[900],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
    ),
    child: Container(
      padding: EdgeInsets.only(left: 5, right: 5),
      height: 30,
      child: Center(
        child: Text('T-SIRTS',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    ),
  );
  Widget shirtLabel = Card(
    color: Colors.blueGrey[900],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
    ),
    child: Container(
      padding: EdgeInsets.only(left: 5, right: 5),
      height: 30,
      child: Center(
        child: Text('T-SIRTS',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    ),
  );

  Widget vastr_favorite = Card(
    color: Colors.blueGrey[900],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
    ),
    child: Container(
      padding: EdgeInsets.only(left: 15, right: 15),
      height: 30,
      child: Center(
        child: Text('VASTR FAVORITES',
          style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold
          ),
          textAlign: TextAlign.center,
        ),
      ),
    ),
  );

  Widget also_expose = Card(
    color: Colors.blueGrey[900],
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8),
    ),
    child: Container(
      padding: EdgeInsets.only(left: 15, right: 15),
      height: 30,
      child: Center(
        child: Text('ALSO EXPOSE',
          style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold
          ),
          textAlign: TextAlign.center,
        ),
      ),
    ),
  );

  Widget favorite_pro = Container(
    height: 250,
    margin: EdgeInsets.only(left: 5, right: 5),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        image: DecorationImage(
          image: NetworkImage('http://www.projects.estateahead.com/vastr/assets/images/all_banner.png'),
          fit: BoxFit.fill,
        )
    ),
  );

  Widget shoppingCart() {
    return Container(
      child: GestureDetector(
        onTap: (){
          Navigator.of(context).pushNamed(MY_CART);
        },
        child: Stack(
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: null,
              iconSize: 30,
            ),
            Positioned(
              right: 5,
              top: 3,
              child: Container(
                width: 21.0,
                height: 21.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  color: Colors.red
                ),
                child: Container(
                  width: 21.5,
                  height: 21.5,
                  child: new Stack(
                    children: <Widget>[
                      new Icon(Icons.brightness_1,
                          size: 21.0, color: Colors.white),
                      new Center(
                        child: new Text('5',
                          /*model.order == null
                              ? '0'
                              : model.order.totalQuantity.toString(),*/
                          style: new TextStyle(
                              color: Colors.black,
                              fontSize: 11.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

