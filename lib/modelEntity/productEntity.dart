import 'package:Vastr/constant/constants.dart';

class ProductEntity {
  final String id,
      proId,
      userId,
      createdAt,
      proName,
      proUrl,
      proCategoryId,
      proSubCategory,
      proChild,
      proSku,
      proPrice,
      proQuantity,
      proSize,
      proScalePrice,
      proNew,
      proArrival,
      metrialId,
      brandId,
      proFeatImage,
      proGalleryImage,
      proSortDesc,
      proFullDesc,
      proMetaContent,
      proColor,
      proDate,
      proPopular,
      proType;

  ProductEntity({this.id, this.proId, this.userId, this.createdAt, this.proName,
    this.proUrl, this.proCategoryId, this.proSubCategory, this.proChild,
    this.proSku, this.proPrice, this.proQuantity, this.proSize,
    this.proScalePrice, this.proNew, this.proArrival, this.metrialId,
    this.brandId, this.proFeatImage, this.proGalleryImage, this.proSortDesc,
    this.proFullDesc, this.proMetaContent, this.proColor, this.proDate,
    this.proPopular, this.proType});

  factory ProductEntity.fromJson(Map<String, dynamic> json) {
    print(json);
    return ProductEntity(
      id: json['id'] as String,
      proId: json['pro_id'] as String,
      userId: json['user_id'] as String,
      createdAt: json['created_at'] as String,
      proName: json['pro_name'] as String,
      proUrl: json['pro_url'] as String,
      proCategoryId: json['pro_category_id'] as String,
      proSubCategory: json['pro_sub_category'] as String,
      proChild: json['pro_child'] as String,
      proSku: json['pro_sku'] as String,
      proPrice: json['pro_price'] as String,
      proQuantity: json['pro_quantity'] as String,
      proSize: json['pro_size'] as String,
      brandId: json['brand_id'] as String,
      proFeatImage: BASE_IMAGE_URL+json['pro_feat_image'] as String,
      proGalleryImage: json['pro_gallery_image'] as String,
      proSortDesc: json['pro_sort_desc'] as String,
      metrialId: json['metrial_id'] as String,
      proArrival: json['pro_arrival'] as String,
      proNew: json['pro_new'] as String,
      proScalePrice: json['pro_sale_price'] as String,
      proFullDesc: json['pro_full_desc'] as String,
      proMetaContent: json['pro_meta_content'] as String,
      proColor: json['pro_color'] as String,
      proDate: json['pro_date'] as String,
      proPopular: json['pro_popular'] as String,
      proType: json['pro_type'] as String,
    );
  }
}


class GetAllProducts {
  final String id;
  final String proId;
  final String proName;
  final String proUrl;
  final String proCategoryId;
  final String proSubCategory;
  final String proChild;
  final String proSKU;
  final String proPrice;
  final String proQuantity;
  final String proSize;
  final String proSalePrice;
  final String proNew;
  final String proArrival;
  final String materialId;
  final String brandId;
  final String proFeatImg;
  final String proGalleryImg;
  final String proSortDesc;
  final String proFullDesc;
  final String proMetaContent;
  final String proColor;
  final String proDate;
  final String proPopular;
  final String proType;

  GetAllProducts({this.id,this.proId, this.proName, this.proUrl, this.proCategoryId,
    this.proSubCategory, this.proChild, this.proSKU, this.proPrice,
    this.proQuantity, this.proSize, this.proSalePrice, this.proNew,
    this.proArrival, this.materialId, this.brandId, this.proFeatImg,
    this.proGalleryImg, this.proSortDesc, this.proFullDesc,
    this.proMetaContent, this.proColor, this.proDate, this.proPopular,
    this.proType});


  factory GetAllProducts.fromJson(Map<String, dynamic> json) {
    return GetAllProducts(
      id: json['id'] as String,
      proId: json['pro_id'] as String,
      proName: json['pro_name'] as String,
      proUrl: json['pro_url'] as String,
      proCategoryId: json['pro_category_id'] as String,
      proSubCategory: json['pro_sub_category'] as String,
      proChild: json['pro_child'] as String,
      proSKU: json['pro_sku'] as String,
      proPrice: json['pro_price'] as String,
      proQuantity: json['pro_quantity'] as String,
      proSize: json['pro_size'] as String,
      proSalePrice: json['pro_sale_price'] as String,
      proNew: json['pro_new'] as String,
      proArrival: json['pro_arrival'] as String,
      materialId: json['metrial_id'] as String,
      brandId: json['brand_id'] as String,
      proFeatImg: BASE_IMAGE_URL+json['pro_feat_image'] as String,
      proGalleryImg: json['pro_gallery_image'] as String,
      proSortDesc: json['pro_sort_desc'] as String,
      proFullDesc: json['pro_full_desc'] as String,
      proMetaContent: json['pro_meta_content'] as String,
      proColor: json['pro_color'] as String,
      proDate: json['pro_date'] as String,
      proPopular: json['pro_popular'] as String,
      proType: json['pro_type'] as String,
    );
  }
}

