import 'package:Vastr/constant/constants.dart';

class CategoryEntity {
  final String id;
  final String categoryName;
  final String categoryImage;
  final String thumbnailUrl;

  CategoryEntity({this.id, this.categoryName, this.categoryImage, this.thumbnailUrl});

  factory CategoryEntity.fromJson(Map<String, dynamic> json) {
    print(json);
    return CategoryEntity(
      id: json['id'] as String,
      categoryName: json['cate_name'] as String,
      categoryImage: BASE_IMAGE_URL+json['category_image'] as String,
      thumbnailUrl: BASE_IMAGE_URL + json['app_cat_image'] as String,
    );
  }
}

class SubCategoryEntity {
  final String id;
  final String categoryId;
  final String subCategoryId;
  final String subChildName;
  final String childImage;

  SubCategoryEntity({this.id, this.categoryId, this.subCategoryId, this.subChildName,
    this.childImage});

  factory SubCategoryEntity.fromJson(Map<String, dynamic> json) {
    print(json);
    return SubCategoryEntity(
      id: json['id'] as String,
      subChildName: json['sub_child_name'] as String,
      subCategoryId: json['sub_id'] as String,
      categoryId: json['category_id'] as String,
      childImage: BASE_IMAGE_URL+json['child_image'] as String,
    );
  }
}




