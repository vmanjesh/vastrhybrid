import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';


class CustomerSupport extends StatelessWidget {

  final String phone = 'tel:+2347012345678';

  _callPhone() async {
    if (await canLaunch(phone)) {
      await launch(phone);
    } else {
      throw 'Could not Call Phone';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: Text(
          'Customer Support',
          style: TextStyle(
            fontSize: 25,
          ),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.blueGrey[900], Colors.black],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              )
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(
              height: 210,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Container(
                    height: 210,
                    padding: EdgeInsets.only(bottom: 25),
                    child: Container(
                      child: Image.asset(
                        "assets/supermarketcart.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Positioned(
                    child: FloatingActionButton(
                      onPressed: () {
                        _callPhone;
                      },
                      heroTag: 'call',
                      child: Icon(Icons.call, size: 35, color: Colors.white,),
                      backgroundColor: Colors.black45,
                    ),
                    bottom: 0,
                    right: 65,
                  ),
                  Positioned(
                    child: FloatingActionButton(
                      onPressed: (){},
                      heroTag: 'location',
                      child: Icon(Icons.location_on, size: 35, color: Colors.white,),
                      backgroundColor: Colors.black45,
                    ),
                    bottom: 0,
                    right: 0,
                  ),

                ],
              )
          ),
          Container(
            height: 200,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black54)
            ),
            child: Column(
              children: <Widget>[
                Container(
                  height: 30,
                  child: Row(
                    children: <Widget>[
                      Container(
                        color: Colors.black,
                        width: 90,
                        alignment: Alignment.center,
                        child: Text("About Us", style: TextStyle(color: Colors.white, fontSize: 18),),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(right: 8, left: 8, top: 3),
                  child: Text("   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                    maxLines: 11,
                    style: TextStyle(fontSize: 12.5),
                    softWrap: true,
                    textDirection: TextDirection.ltr,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 95,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black54)
            ),
            child: Column(
              children: <Widget>[
                Container(
                  height: 30,
                  child: Row(
                    children: <Widget>[
                      Container(
                        color: Colors.black,
                        width: 90,
                        alignment: Alignment.center,
                        child: Text("Our Office", style: TextStyle(color: Colors.white, fontSize: 18),),
                      )
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8),
                      child: Text("Vastr Clothing Pvt.Ltd\nSurat Gujrat - 335009\nIndia",
                        maxLines: 11,
                        style: TextStyle(fontSize: 12.5),
                        softWrap: true,
                        textAlign: TextAlign.start,
                        textDirection: TextDirection.ltr,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
