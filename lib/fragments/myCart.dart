import 'package:flutter/material.dart';

class MyCart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text('My Cart'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.blueGrey[900], Colors.black],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              )
          ),
        ),
      ),
      backgroundColor: Colors.blueGrey[900],
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.blueGrey[900],
        ),
        height: 50,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                height: 50,
                color: Colors.white,
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 50,),
                    Text("Total: "),
                    Text("₹ 500"),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.blueGrey[900],
                child: GestureDetector(
                  onTap: () {
                  },
                  child: Center(
                    child: Text(
                      "Process to Checkout".toUpperCase(),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(top: 25),
        children: <Widget>[
          Container(
            height: 250,
            child: Column(
              children: <Widget>[
                Container(
                    height: 150,
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: 5,
                        ),
                        Stack(
                          children: <Widget>[
                            Container(
                              height: 100,
                              width: 100,
                              child: Container(
                                  height: 90,
                                  width: 90,
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: 15,
                                      ),
                                      Image.network(
                                        "https://www.airtel.in/assets/images/offer_banner_graphic.png",
                                        height: 85,
                                        width: 85,
                                      ),
                                    ],
                                  )
                              ),
                            ),
                            Positioned(
                              child: Container(
                                height: 40,
                                width: 40,
                                child: CircleAvatar(
                                  backgroundColor: Colors.black,
                                  child: Text(
                                    " % \nOff",
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                              ),
                              top: 0,
                              left: 0,
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 25,
                        ),
                        Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: 170,
                                child: Text(
                                    "Test",
                                    style: TextStyle(fontSize: 18,),
                                    textAlign: TextAlign.start
                                ),
                              ),
                              SizedBox(
                                height: 50,
                              ),
                              Text(
                                'Discount benifit : Flat % off',
                                style: TextStyle(color: Colors.green),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                ),
                Container(
                  height: 1,
                  color: Colors.black45,
                ),
                Container(
                  height: 38,
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 180,
                        height: 30,
                        /*decoration: BoxDecoration(
                          border: Border(
                            right: BorderSide(color: Colors.black)
                          ),
                        ),*/
                      ),
                      Container(
                        width: 180,
                        height: 30,
                        decoration: BoxDecoration(
                            border: Border(
                                left: BorderSide(color: Colors.black45)
                            )
                        ),
                        child: GestureDetector(
                          onTap: () {
                            print("hello");
                          },
                          child: Row(
                            children: <Widget>[
                              SizedBox(width: 35,),
                              Container(
                                height: 20,
                                width: 20,
                                child: CircleAvatar(
                                  backgroundColor: Colors.black,
                                  child: Icon(
                                    Icons.check,
                                    size: 18,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 12,
                              ),
                              Text(
                                  "Copy Code"
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1,
                  color: Colors.black45,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
