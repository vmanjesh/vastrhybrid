import 'dart:io';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';


class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {

  final TextEditingController userController = TextEditingController();
  final TextEditingController mobileController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController stateController = TextEditingController();
  final TextEditingController cityController = TextEditingController();

//  <=================================Image Choice Section==================================>



  File _imageFile;
  _openGallery(BuildContext context) async{
    var picture = await ImagePicker.pickImage(source: ImageSource.gallery);
    this.setState((){
      _imageFile = picture;
    });
    Navigator.of(context).pop();
  }

  _openCamera(BuildContext context) async{
    var picture = await ImagePicker.pickImage(source: ImageSource.camera);
    this.setState((){
      _imageFile = picture;
    });

    Navigator.of(context).pop();
  }

  Future<void> _showChoiceDialog(BuildContext context){
    return showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        title: Text('Make a Choice!'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              GestureDetector(
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.image,
                      color: Colors.blueAccent,
                    ),
                    SizedBox(width: 10,),
                    Text('Gallery'),
                  ],
                ),
                onTap: (){
                  _openGallery(context);
                },
              ),
              Padding(
                padding: EdgeInsets.all(8),
              ),
              GestureDetector(
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.camera,
                      color: Colors.lightBlue,
                    ),
                    SizedBox(width: 10,),
                    Text('Camera'),
                  ],
                ),
                onTap: (){
                  _openCamera(context);
                },
              )
            ],
          ),
        ),
      );
    });
  }

  Widget _decideImage() {
    if(_imageFile == null){
      return Text('No Image selected!');
    }
    else{
      Image.file(_imageFile, width: 130, height: 130,);
    }
  }

//  <========================Date of Birth Section============================>

  DateTime _currentDate = DateTime.now();
  TextEditingController _date = new TextEditingController();
  Future<Null> _selectdate(BuildContext context) async{
    final DateTime _seldate = await showDatePicker(
        context: context,
        initialDate: _currentDate,
        firstDate: DateTime(1990),
        lastDate: DateTime(2021),
        builder: (context,child) {
          return SingleChildScrollView(child: child,);
        }
    );
    String _formattedate = new DateFormat.yMd().format(_currentDate);
    if (_seldate != null && _seldate != _currentDate)
      setState(() {
        _currentDate = _seldate;
        _date.value = TextEditingValue(text: _seldate.toString().replaceAll('$_seldate', '$_formattedate'));
      });
  }
  int _value = 0;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: Text(
          'My Profile',
          style: TextStyle(
            fontSize: 25,
          ),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.black45, Colors.black],
                stops: [0.1, 0.9],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              )
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          child: Center(
            child: ListView(
              padding: EdgeInsets.only(left: 35, right: 35),
              children: <Widget>[
                SizedBox(height: 15,),
//                profile_image,


//              <---------------------Profile Section------------------->


                Container(
                  child: Center(
                    child: Stack(
                      children: <Widget>[
                        CircleAvatar(
                          radius: 66,
//                  backgroundColor: Color(0xff476cfb),
                          child: ClipOval(
                            child: SizedBox(
                              height: 140,
                              width: 140,
                              child: (_imageFile!=null)
                                  ?Image.file(
                                _imageFile,
                                fit: BoxFit.fill,
                              )
                              /*:Image.network(
                        "https://therminic2018.eu/wp-content/uploads/2018/07/dummy-avatar-300x300.jpg",
                        fit: BoxFit.fill,
                      ),*/
                                  :Container(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 7,
                          child: Container(
                            height: 35,
                            width: 35,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: Colors.black),
                                color: Colors.white

                            ),
                            child: IconButton(
                              alignment: Alignment.center,
                              color: Colors.black26,
                              icon: Icon(Icons.mode_edit,
                                color: Colors.black,
                                size: 25,
                              ),
                              onPressed: () {
                                _showChoiceDialog(context);
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
//                SizedBox(height: 10,),

//              <-------------------UserDetails--------------------->
//                user_details,
                Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: userController,
                          decoration: InputDecoration(
                            labelText: "User Name",
                            labelStyle: TextStyle(
                                color: Colors.white
                            ),
                            fillColor: Colors.white,
                          ),
                          style: TextStyle(height: 1, color: Colors.white),
                          keyboardType: TextInputType.text,
                        ),
                        TextFormField(
                          controller: mobileController,
                          decoration: InputDecoration(
                            labelText: "Phone Number",
                            labelStyle: TextStyle(
                                color: Colors.white
                            ),
                            fillColor: Colors.white,
                          ),
                          style: TextStyle(height: 1, color: Colors.white),
                          keyboardType: TextInputType.number,
                        ),
                        TextFormField(
                          controller: emailController,
                          decoration: InputDecoration(
                            labelText: "Email ID",
                            labelStyle: TextStyle(
                                color: Colors.white
                            ),
                            fillColor: Colors.white,
                          ),
                          style: TextStyle(height: 1, color: Colors.white),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () => _selectdate(context),
                  child: AbsorbPointer(
                    child: TextFormField(
                      style: TextStyle(height: 1, color: Colors.white),
                      controller: _date,
                      keyboardType: TextInputType.datetime,
                      decoration: InputDecoration(
                        labelText: 'Date of Birth',
                        labelStyle: TextStyle(
                            color: Colors.white
                        ),
                      ),
                    ),
                  ),
                ),
//                user_detailss,
                Container(

                  child: Center(
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: stateController,
                          decoration: InputDecoration(
                            labelText: "Select State",
                            labelStyle: TextStyle(
                                color: Colors.white
                            ),
                            fillColor: Colors.white,
                          ),
                          style: TextStyle(height: 1, color: Colors.white),
                          keyboardType: TextInputType.text,
                        ),
                        TextFormField(
                          controller: cityController,
                          decoration: InputDecoration(
                            labelText: "Select City",
                            labelStyle: TextStyle(
                                color: Colors.white
                            ),
                            fillColor: Colors.white,
                          ),
                          style: TextStyle(height: 1, color: Colors.white),
                          keyboardType: TextInputType.text,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10,),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        'Gender',
                        style: TextStyle(fontSize: 17, color: Colors.white),
                      ),
                    ),
                    GestureDetector(
                      onTap: () => setState(() => _value = 0),
                      child: Container(
                        height: 35,
                        width: 70,
//                        color: _value == 1 ? Colors.grey : Colors.transparent,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(16)),
                            color: _value == 1 ? Colors.transparent : Colors.grey[350],
                            border: Border.all(color: Colors.black)
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Male',
                              style: TextStyle(fontSize: 10),
                            ),
                            Icon(
                                FontAwesomeIcons.male
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    GestureDetector(
                      onTap: () => setState(() => _value = 1),
                      child: Container(
                        height: 35,
                        width: 70,
//                        color: _value == 1 ? Colors.grey : Colors.transparent,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(16)),
                            color: _value == 1 ? Colors.grey[350] : Colors.transparent,
                            border: Border.all(color: Colors.black)
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Female',
                              style: TextStyle(fontSize: 10),
                            ),
                            Icon(
                                FontAwesomeIcons.female
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Container(
                  height: 45,
                  child: RaisedButton(
                    onPressed: (){},
                    textColor: Colors.white,
                    splashColor: Colors.lightGreen,
                    padding: EdgeInsets.all(0.0),
                    child: Container(
                      width: 300,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [Colors.black54, Colors.black],
//                            stops: [0.1, 0.9],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          )
                      ),
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      child: const Text(
                        'SUBMIT',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
