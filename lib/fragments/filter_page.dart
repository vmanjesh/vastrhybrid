import 'package:flutter/material.dart';

class FilterPage extends StatefulWidget {
  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Filter'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.blueGrey[900], Colors.black],
//              stops: [0.3, 1]
              )
          ),
        ),
      ),
//      backgroundColor: Colors.blueGrey[900],
      body: SafeArea(
        child: Container(
            padding: EdgeInsets.only(top: 3),
            color: Colors.black,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 9,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 5,),
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15,),
                                      Text('DISCOUNT', style: TextStyle(color: Colors.white, fontSize: 15),)
                                    ],
                                  ),
                                ),
                              ),
                              Divider(color: Colors.white,),
                              SizedBox(height: 5,),
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15,),
                                      Text('PRICE RANGE', style: TextStyle(color: Colors.white, fontSize: 15),)
                                    ],
                                  ),
                                ),
                              ),
                              Divider(color: Colors.white,),
                              SizedBox(height: 5,),
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15,),
                                      Text('SIZE =', style: TextStyle(color: Colors.white, fontSize: 15),)
                                    ],
                                  ),
                                ),
                              ),
                              Divider(color: Colors.white,),
                              SizedBox(height: 5,),
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      SizedBox(width: 15,),
                                      Text('CATEGORIES =', style: TextStyle(color: Colors.white, fontSize: 15),)
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 5,),
                              Divider(color: Colors.white,),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.white,
                        width: 1,
                      ),
                      Expanded(
                        flex: 5,
                        child: Container(

                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    margin: EdgeInsets.only(top: 5),
                    color: Colors.white,
                    child: Row(
                      children: <Widget>[
                        GestureDetector(
                          child: Container(
                            width: 180,
                            child: Center(child: Text('Close', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)),
                          ),
                        ),
                        Container(
                          color: Colors.grey,
                          width: 1,
                          margin: EdgeInsets.only(top: 8, bottom: 8),
                        ),
                        GestureDetector(
                          child: Container(
                            width: 179,
                            child: Center(child: Text('Apply', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }
}
