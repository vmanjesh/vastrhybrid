import 'package:flutter/material.dart';
import 'package:Vastr/constant/constants.dart';


class MyAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          'My Address',
          style: TextStyle(
            fontSize: 25,
          ),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.blueGrey[900], Colors.black],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              )
          ),
        ),
      ),
      body: Container(
        color: Colors.blueGrey[900],
        child: ListView(
          children: <Widget>[
            Container(
              height: 172,
              color: Colors.transparent,
              margin: EdgeInsets.only(top: 25, left: 15, right: 15),
              child: Column(
                children: <Widget>[
                  Container(
                      height: 117,
                      color: Colors.transparent,
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 5,
                          ),
                          Container(
                            height: 100,
                            width: 100,
                            child: Image.network(
                              "https://www.paidmembershipspro.com/wp-content/uploads/2014/02/Shipping-Address.png",
                              height: 85,
                              width: 85,
                            ),
                          ),
                          SizedBox(
                            width: 25,
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: 170,
                                  child: Text(
                                      "Test",
                                      style: TextStyle(fontSize: 18,),
                                      textAlign: TextAlign.start
                                  ),
                                ),
                                SizedBox(
                                  height: 50,
                                ),
                                Text(
                                  'Discount benifit : Flat % off',
                                  style: TextStyle(color: Colors.green),
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                    height: 1,
                    color: Colors.white,
                  ),
                  Container(
                    height: 38,
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: 165,
                          height: 30,

                        ),
                        Container(
                          width: 165,
                          height: 30,
                          decoration: BoxDecoration(
                              border: Border(
                                  left: BorderSide(color: Colors.white)
                              )
                          ),
                          child: GestureDetector(
                            onTap: () {
                              print("hello");
                            },
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: 20,),
                                Center(
                                  child: Container(
                                    height: 20,
                                    width: 20,
                                    child: Icon(
                                      Icons.check,
                                      size: 25,
                                      color: Colors.green,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 12,
                                ),
                                Text(
                                  "Copy Code",
                                  style: TextStyle(color: Colors.green),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 1,
                    color: Colors.white,
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: "addAddress",
        onPressed: () {
//          Navigator.of(context).pop();
          Navigator.of(context).pushNamed(ADD_ADDRESS);
        },
        backgroundColor: Colors.black,
        child: Icon(
          Icons.add,
          size: 35,
        ),
      ),
    );
  }
}
