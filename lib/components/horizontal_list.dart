import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:Vastr/constant/constants.dart';
import 'package:Vastr/modelEntity/categoryEntity.dart';
import 'package:Vastr/modelEntity/productEntity.dart';


final url = BASE_URL + 'all_category';
Future<List<CategoryEntity>> fetchCategory(http.Client client) async {
  final response = await client.get('http://www.projects.estateahead.com/vastr/api/all_category');
  // Use the compute function to run parsePhotos in a separate isolate.
  return compute(parseCategory, response.body);
}

// A function that converts a response body into a List<Photo>.
List<CategoryEntity> parseCategory(String responseBody) {
  final parsed = jsonDecode(responseBody)['response'].cast<Map<String, dynamic>>();
  return parsed.map<CategoryEntity>((json) => CategoryEntity.fromJson(json)).toList();
}

class HorizontalList extends StatefulWidget {
  @override
  _HorizontalListState createState() => _HorizontalListState();
}
class _HorizontalListState extends State<HorizontalList> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<List<CategoryEntity>>(
        future: fetchCategory(http.Client()),
        builder: (context, snapshot) {
//          return MyList(category: snapshot.data,);
          return snapshot.hasData
              ? MyList(category: snapshot.data,)
              : Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class MyList extends StatelessWidget {
  final List<CategoryEntity> category;

  const MyList({Key key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 95,
      margin: EdgeInsets.only(left: 2, right: 5),
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: category.length,
        itemBuilder: (context, index) {
          return Container(
            child: GestureDetector(
              onTap: () {},
              child: Container(
                padding: EdgeInsets.only(top: 15, bottom: 10, left: 3, right: 3),
                height: 90,
                width: 130,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  child: Image.network(
                    category[index].thumbnailUrl,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class ProductsExpo extends StatefulWidget {
  @override
  _ProductsExpoState createState() => _ProductsExpoState();
}

class _ProductsExpoState extends State<ProductsExpo> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: FutureBuilder<List<CategoryEntity>>(
        future: fetchCategory(http.Client()),
        builder: (context, snapshot) {
          if(snapshot.hasError){
            print(snapshot.error);
          }
          return snapshot.hasData
              ? Expo_Pro(category: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class Expo_Pro extends StatelessWidget {

  final List<CategoryEntity> category;

  Expo_Pro({this.category});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 3,
          mainAxisSpacing: 3,
          childAspectRatio: 0.98
      ),
      primary: false,
      itemCount: category.length,
      itemBuilder: (context, index) {
        return Container(
            height: 150,
            width: 90,
            child: Column(
              children: <Widget>[
                Container(
                  height: 80,
                  width: 80,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    child: Image.network(
                      category[index].categoryImage,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                SizedBox(height: 10,),
                Text(
                  category[index].categoryName.toUpperCase(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            )
        );
      },
    );
  }
}


final url2 = BASE_URL + "get_latest_product";
Future<List<ProductEntity>> fetchProCategory(http.Client client) async {
  final response = await client.get(url2);

  print(jsonDecode(response.body)['data']);
  // Use the compute function to run parsePhotos in a separate isolate.
  return compute(parseProCategory, response.body);
}

// A function that converts a response body into a List<Photo>.
List<ProductEntity> parseProCategory(String responseBody) {
  final parsed = jsonDecode(responseBody)['data'].cast<Map<String, dynamic>>();
  return parsed.map<ProductEntity>((json) => ProductEntity.fromJson(json)).toList();
}

class HistoryProduct extends StatefulWidget {
  @override
  _HistoryProductState createState() => _HistoryProductState();
}

class _HistoryProductState extends State<HistoryProduct> {

  double percent;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<List<ProductEntity>>(
        future: fetchProCategory(http.Client()),
        builder: (context, snapshot) {
          return snapshot.hasData
              ? ProductList(category: snapshot.data,)
              : Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class ProductList extends StatelessWidget {

  final List<ProductEntity> category;
  ProductList({this.category});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: category.length,
        itemBuilder: (context, index) {
          return Container(
            padding: EdgeInsets.all(5),
            child: GestureDetector(
              onTap: () {},
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Colors.white
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10, left: 2, right: 2),
                      height: 200,
                      width: 130,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        child: Image.network(
                          category[index].proFeatImage,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),

                    Container(
                      height: 40,
                      width: 130,
                      padding: EdgeInsets.only(left: 8),
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8), bottomRight: Radius.circular(8)),
                      ),
                      child: Column(
                        children: <Widget>[
                          Text(
                            category[index].proName.toUpperCase(),
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 17,
                            ),
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Row(
                            children: <Widget>[
                              Text("₹ "+category[index].proPrice, style: TextStyle(color: Colors.black45,decoration: TextDecoration.lineThrough),),
                              SizedBox(width: 25,),
                              Text("₹ "+category[index].proScalePrice, style: TextStyle(color: Colors.black),)
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}




