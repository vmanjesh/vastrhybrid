import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class AddAddress extends StatefulWidget {
  @override
  _AddAddressState createState() => _AddAddressState();
}

class _AddAddressState extends State<AddAddress> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: this._scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0.0,
      ),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        child: Center(
          child: Container(
            margin: EdgeInsets.only(top: 145),
            height: 600,
            width: 300,
            child: Column(
              children: <Widget>[
                Text(
                  "Hi, nice to meet you !",
                  style: TextStyle(
                      fontSize: 20
                  ),
                ),
                SizedBox(
                  height: 22,
                ),
                Text(
                  "Add new Address",
                  style: TextStyle(
                      fontSize: 30
                  ),
                ),
                SizedBox(
                  height: 29,
                ),
                Image.asset(
                    "assets/city.png"
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: 43,
                  width: 250,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(3)),
                    border: Border.all(color: Colors.black),
                    color: Colors.black,
                  ),
                  child: RaisedButton(
                    onPressed: () =>this._scaffoldKey
                        .currentState
                        .showBottomSheet((cxt) => _buildBottomSheet(cxt)),
                    color: Colors.black,
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: 10,),
                        Icon(Icons.my_location, color: Colors.white,),
                        SizedBox(width: 8,),
                        Text(
                          "Your Currrent Location".toUpperCase(),
                          style: TextStyle(
                              color: Colors.white, fontSize: 13
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 28,
                ),
                GestureDetector(
                  child: Container(
                    height: 43,
                    width: 250,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                      border: Border.all(color: Colors.black),
                      color: Colors.white,
                    ),
                    child: Center(
                      child: Row(
                        children: <Widget>[
                          SizedBox(width: 25,),
                          Icon(Icons.search),
                          SizedBox(width: 10,),
                          Container(
                            width: 180,
                            child: Text(
                              "Some other Location".toUpperCase(),
                              style: TextStyle(
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  int id = 1;
}

Container _buildBottomSheet(BuildContext context){

  return Container(
    child: ListView(
//      reverse: true,
      children: <Widget>[
        Container(
          height: 800,
          child: Column(
            children: <Widget>[
              GestureDetector(
                onTap: (){
                  Navigator.of(context, rootNavigator: true).pop();
                },
                child: Container(
                  height: 319,
                  color: Colors.black45,
                ),
              ),
              SizedBox(
                height: 1,
              ),
              Container(
                height: 400,
                child: ListView(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(10),
                      height: 20,
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Add Delivery Address",
                            style: TextStyle(fontSize: 20),

                          ),
                          Container(
                            width: 150,
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 20,
                      child: RadioButtonGroup(
                        picked: "Mr.",
                        orientation: GroupedButtonsOrientation.HORIZONTAL,
                        labels: <String>[
                          "Mr.",
                          "Mrs.",
                          "Miss.",
                        ],
                        onSelected: (String selected) => print(selected),
                        itemBuilder: (Radio rb, Text txt, int i){
                          return Row(
                            children: <Widget>[
                              rb,
                              txt,
                            ],
                          );
                        },
                      ),
                    ),
                    Container(
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: 50,
                              width: 325,
                              child: TextFormField(
                                decoration: InputDecoration(
                                  hintText: "User Name",
                                ),
                                style: TextStyle(height: 1,),
                                keyboardType: TextInputType.text,
                              ),
                            ),
                            Container(
                              height: 50,
                              width: 325,
                              child: TextFormField(
                                decoration: InputDecoration(
                                  hintText: "User Name",
                                ),
                                style: TextStyle(height: 1,),
                                keyboardType: TextInputType.text,
                              ),
                            ),
                            Container(
                              height: 50,
                              width: 325,
                              child: TextFormField(
                                decoration: InputDecoration(
                                  hintText: "User Name",
                                ),
                                style: TextStyle(height: 1,),
                                keyboardType: TextInputType.text,
                              ),
                            ),
                            Container(
                              height: 50,
                              width: 325,
                              child: TextFormField(
                                decoration: InputDecoration(
                                  hintText: "User Name",
                                ),
                                style: TextStyle(height: 1,),
                                keyboardType: TextInputType.text,
                              ),
                            ),
                            Container(
                              height: 50,
                              width: 325,
                              child: TextFormField(
                                decoration: InputDecoration(
                                  hintText: "User Name",
                                ),
                                style: TextStyle(height: 1,),
                                keyboardType: TextInputType.text,
                              ),
                            ),
                            SizedBox(height: 10,),
                            Row(
                              children: <Widget>[
                                SizedBox(width: 15,),
                                Container(
                                  height: 25,
                                  child: Text("Nick Name of Your Address"),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
