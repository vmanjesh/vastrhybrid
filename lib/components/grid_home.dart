import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:Vastr/constant/constants.dart';
import 'package:Vastr/modelEntity/categoryEntity.dart';
import 'categoryList.dart';



final url = BASE_URL+"all_sub_subchild/1/1";
Future<List<SubCategoryEntity>> fetchCategory(http.Client client) async {
  final response = await client.get(url);

  // Use the compute function to run parsePhotos in a separate isolate.
  return compute(parseCategory, response.body);
}

final url1 = BASE_URL+"all_sub_subchild/2/1";
Future<List<SubCategoryEntity>> fetchSubCategory(http.Client client) async {
  final response = await client.get(url1);

  // Use the compute function to run parsePhotos in a separate isolate.
  return compute(parseCategory, response.body);
}

// A function that converts a response body into a List<Photo>.
List<SubCategoryEntity> parseCategory(String responseBody) {
  final parsed = jsonDecode(responseBody)['response'].cast<Map<String, dynamic>>();
  return parsed.map<SubCategoryEntity>((json) => SubCategoryEntity.fromJson(json)).toList();
}

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: FutureBuilder<List<SubCategoryEntity>>(
        future: fetchCategory(http.Client()),
        builder: (context, snapshot) {
          if(snapshot.hasError){
            print(snapshot.error);
          }
          return snapshot.hasData
              ? Single_Pro(category: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class Single_Pro extends StatelessWidget {

  final List<SubCategoryEntity> category;

  Single_Pro({this.category});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 5,
          crossAxisSpacing: 3,
          childAspectRatio: 0.7,
        ),
        primary: false,
        itemCount: category.length,
        itemBuilder: (context, index) {
          return Container(
            height: 130,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.blueGrey[900],
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.black,
                  height: 130,
                  margin: EdgeInsets.all(5),
                  child: InkWell(
                    onTap: () =>
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => CategoryList())),
                    child: Image.network(
                      category[index].childImage, fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 5,),
                Text(category[index].subChildName, style: TextStyle(color: Colors.white),)
              ],
            ),
          );
        }
    );
  }
}
class Products2 extends StatefulWidget {
  @override
  _Products2State createState() => _Products2State();
}

class _Products2State extends State<Products2> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: FutureBuilder<List<SubCategoryEntity>>(
        future: fetchSubCategory(http.Client()),
        builder: (context, snapshot) {
          if(snapshot.hasError){
            print(snapshot.error);
          }
          return snapshot.hasData
              ? SinglePro(category: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class SinglePro extends StatelessWidget {

  final List<SubCategoryEntity> category;

  SinglePro({this.category});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 5,
          crossAxisSpacing: 3,
          childAspectRatio: 0.7,
        ),
        primary: false,
        itemCount: category.length,
        itemBuilder: (context, index) {
          return Container(
            height: 130,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.blueGrey[900],
              borderRadius: BorderRadius.all(Radius.circular(5)),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.black,
                  height: 130,
                  margin: EdgeInsets.all(5),
                  child: InkWell(
                    onTap: () =>
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => CategoryList())),
                    child: Image.network(
                      category[index].childImage, fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 5,),
                Text(category[index].subChildName, style: TextStyle(color: Colors.white),)
              ],
            ),
          );
        }
    );
  }
}
