import 'dart:convert';
import 'package:Vastr/pages/product_details.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:Vastr/constant/constants.dart';
import 'package:Vastr/modelEntity/productEntity.dart';
import 'package:Vastr/fragments/filter_page.dart';


final url = BASE_URL + 'all_category';
Future<List<GetAllProducts>> fetchCategory(http.Client client) async {
  final response = await client.get(url);
  print(jsonDecode(response.body)['data']);
  // Use the compute function to run parsePhotos in a separate isolate.
  return compute(parseCategory, response.body);
}

// A function that converts a response body into a List<Photo>.
List<GetAllProducts> parseCategory(String responseBody) {
  final parsed = jsonDecode(responseBody)['data'].cast<Map<String, dynamic>>();
  return parsed.map<GetAllProducts>((json) => GetAllProducts.fromJson(json)).toList();
}

class GetAllProduct extends StatefulWidget {
  @override
  _GetAllProductState createState() => _GetAllProductState();
}

class _GetAllProductState extends State<GetAllProduct> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: FutureBuilder<List<GetAllProducts>>(
        future: fetchCategory(http.Client()),
        builder: (context, snapshot) {
          if(snapshot.hasError){
            print(snapshot.error);
          }
          return snapshot.hasData
              ? /*_CategoryListState(category: snapshot.data)*/ Text("Hello")
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class CategoryList extends StatefulWidget {
  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {

  final List<GetAllProducts> category;
  _CategoryListState({this.category});

  bool _isSelect = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.black,
        title: Text('Products',
          style: TextStyle(
              fontSize: 25,
              color: Colors.white
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pushNamed(MY_CART);
            },
            iconSize: 30,
          ),
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {},
            iconSize: 30,
          ),
        ],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Colors.blueGrey[900],
          border: Border(
            top: BorderSide(color: Colors.white54)
          )
        ),
        height: 50,
        alignment: Alignment.center,

        child: Row(
          children: <Widget>[
            RaisedButton(
              padding: EdgeInsets.all(0.0),
              onPressed: (){},
              child: Ink(
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.blueGrey[900],
                    /*border: Border(
                      top: BorderSide(color: Colors.white),
                    )*/
                ),
                child: Container(
                  constraints: BoxConstraints(maxWidth: 119),
                  alignment: Alignment.center,
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 20,),
                      Text(
                        'GENDER',
                        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white,),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 6, bottom: 6),
              width: 1,
            ),
            RaisedButton(
              padding: EdgeInsets.all(0.0),
              onPressed: (){
                _settingModalBottomSheet(context);
              },
              child: Ink(
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.blueGrey[900],
                    /*border: Border(
                      top: BorderSide(color: Colors.white),
                    )*/
                ),
                child: Container(
                  constraints: BoxConstraints(maxWidth: 119),
                  alignment: Alignment.center,
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 20,),
                      Icon(Icons.sort, color: Colors.white,),
                      Text(
                        'SORT',
                        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white,),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.white,
              margin: EdgeInsets.only(top: 6, bottom: 6),
              width: 1,
            ),
            RaisedButton(
              padding: EdgeInsets.all(0.0),
              onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => FilterPage()));
              },
              child: Ink(
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.blueGrey[900],
                    /*border: Border(
                      top: BorderSide(color: Colors.white),
                    )*/
                ),
                child: Container(
                  constraints: BoxConstraints(maxWidth: 118),
                  alignment: Alignment.center,
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 20,),
                      FaIcon(FontAwesomeIcons.filter, size: 15, color: Colors.white,),
                      Text(
                        ' FILTER',
                        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white,),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      body: Container(
        color: Colors.blueGrey[900],
        child: StaggeredGridView.countBuilder(
          crossAxisCount: 4,
          itemCount: 8,
          itemBuilder: (BuildContext context, int index){
            return GestureDetector(
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProductDetails()));
              },
//                        onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProductDetails())),
              child: Container(
                height: 300,
                width: 177,
                /*decoration: BoxDecoration(
                    border: Border.all(color: Colors.red)
                  ),*/
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 230,
                      width: 177,
                      child: Image.network(
//                                  category[index].proFeatImg, fit: BoxFit.fill,
                        BASE_IMAGE_URL+"IMG-20200305-WA0047.jpg", fit: BoxFit.fill,
                      ),
                    ),
                    Container(
                      height: 69,
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(color: Colors.black)
                          ),
                          color: Colors.blueGrey[900]
                      ),
                      child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 3,
                            ),
                            Container(
                              child: Row(
                                children: <Widget>[
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Container(
                                    width: 122,
                                    child: Text(
//                                              category[index].proName,
                                      "Black",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Container(
                                    width: 40,
                                    child: InkWell(
                                      onTap: () {
                                        setState(() =>
                                        _isSelect = !_isSelect
                                        );
                                        print("object");
                                      },
                                      child: Icon(
                                          _isSelect
                                              ? Icons.favorite
                                              : Icons.favorite,
                                        color: _isSelect ? Colors.white : Colors.grey,
                                        ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                                child: Row(
                              children: <Widget>[
                                SizedBox(width: 8,),
                                Text(
                                  "DSADSDASA",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12
                                  ),
                                ),
                              ],
                            )
                            ),
                            Container(
                                child: Row(
                              children: <Widget>[
                                SizedBox(width: 8,),
                                Text(
                                  '₹ 500',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.lineThrough,
                                      fontSize: 12
                                  ),
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                Text(
                                  '₹ 500',
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12
                                  ),
                                ),
                              ],
                            )
                            ),
                          ],
                        ),
                    ),
                  ],
                ),
              ),
            );
          },
          staggeredTileBuilder: (int index) =>
              StaggeredTile.fit(2),
        ),
      ),
    );
  }
}

void _settingModalBottomSheet(context){
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc){
        return Container(
          decoration: BoxDecoration(
//            borderRadius: BorderRadius.only(topRight: Radius.circular(7), topLeft: Radius.circular(7)),
            color: Colors.black,
          ),

          child: ListView(
            shrinkWrap: true,
            primary: false,
            children: <Widget>[
              SizedBox(height: 5,),
              Container(
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 15,),
                    Text('SORT BY', style: TextStyle(color: Colors.white),),
                  ],
                ),
              ),
              SizedBox(height: 10,),

              Container(color: Colors.white, height: 1, padding: EdgeInsets.only(left: 8, right: 8),),
              SizedBox(height: 10,),
              Container(
                child: Text('SORT BY', style: TextStyle(color: Colors.white),),
              ),
              SizedBox(height: 10,),
              Container(
                child: Text('SORT BY', style: TextStyle(color: Colors.white),),
              ),
              SizedBox(height: 10,),
              Container(
                child: Text('SORT BY', style: TextStyle(color: Colors.white),),
              ),
              SizedBox(height: 10,),

            ],
          ),
        );
      }
  );
}
