import 'package:flutter/material.dart';
import 'components/addressLocation.dart';
import 'constant/constants.dart';
import 'fragments/customerSupports.dart';
import 'fragments/myAddress.dart';
import 'fragments/myCart.dart';
import 'fragments/myOrders.dart';
import 'fragments/myProfile.dart';
import 'fragments/offerZone.dart';
import 'fragments/termsNconditions.dart';
import 'pages/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      routes: <String, WidgetBuilder>{
        HOME_PAGE: (BuildContext context) => HomePage(),
        ADD_ADDRESS: (BuildContext context) => AddAddress(),
        MY_ADDRESS: (BuildContext context) => MyAddress(),
        MY_CART: (BuildContext context) => MyCart(),
        MY_PROFILE: (BuildContext context) => MyProfile(),
        MY_ORDER: (BuildContext context) => MyOrders(),
        CUST_SPRT: (BuildContext context) => CustomerSupport(),
        OFFER_ZONE: (BuildContext context) => OfferZone(),
        TERMS_CDN: (BuildContext context) => TermsCondition(),

      },
    );
  }
}
